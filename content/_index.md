## The OER Cube

This page will basicly answer the following questions

* [Open Education](page/01-title)
* [Why open education?](page/02-why)
* [Who can benefit from OER?](page/03-who)
* [Definition of OER](page/04-what)
* [How to to find and produce](page/05-how)
* [When to start?](page/06-when)
* [Find OER](page/07-find)
* [Remix OER](page/08-remix)
* [Tag OER](page/09-tag)
* [License OER](page/10-license)
* [Publish OER](page/11-publish)
* [Congratulation!](page/12-congrat)

Beside those topics

* [An example OER infrastructure](page/infrastructure)
* [Library services for OER](page/library-services)
