---
title: Library services
subtitle: What a library can provide for OER
date: 2019-07-12
tags: ["services", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.
