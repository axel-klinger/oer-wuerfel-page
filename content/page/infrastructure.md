---
title: Example infrastructure
subtitle: An example Repository - LMS - Content infrastructure
date: 2019-07-12
tags: ["infrastructure", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.
