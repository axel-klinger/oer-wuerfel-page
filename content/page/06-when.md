---
title: When can I start
subtitle: Right now
date: 2019-07-06
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_6.jpg" width="400px" >}}

<!--more-->
