---
title: Who could benefit from OER
subtitle: Who can use and produce OER
date: 2019-07-03
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_3.jpg" width="400px" >}}

<!--more-->
