---
title: Why use and produce OER
subtitle: Why should we open our educational resources
date: 2019-07-02
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_2.jpg" width="400px" >}}

<!--more-->
