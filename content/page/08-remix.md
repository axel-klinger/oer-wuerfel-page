---
title: Remix OER
subtitle: Create a course or course module
date: 2019-07-08
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_8.jpg" width="400px" >}}

<!--more-->
