---
title: Publish OER
subtitle: Make it findable in a global repository
date: 2019-07-11
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_11.jpg" width="400px" >}}

<!--more-->
