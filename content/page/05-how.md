---
title: How to use and produce OER
subtitle: A short example workflow
date: 2019-07-05
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_5.jpg" width="400px" >}}

<!--more-->
