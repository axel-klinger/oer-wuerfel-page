---
title: Open Education for all!
subtitle: A global demand
date: 2019-07-01
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_.jpg" width="400px" >}}

<!--more-->

## A short introduction

{{< vimeo 43401199 >}}
