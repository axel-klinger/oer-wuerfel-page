---
title: Tag with metadata OER
subtitle: Make it findable by categories
date: 2019-07-09
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_9.jpg" width="400px" >}}

<!--more-->
