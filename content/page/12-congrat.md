---
title: Congratulation
subtitle: You are ready to use and produce your first OER
date: 2019-07-12
tags: ["open educational resources", "oer"]
---

Open educational resources as defined by UNESCO have the capability to support quality education for all as targeted by the sustainable development goals.

{{< figure src="/images/IFLA_Wuerfel_12.jpg" width="400px" >}}

<!--more-->
